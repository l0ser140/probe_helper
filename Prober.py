try:
    from traceback import format_exception
    import uix
    import uiutil
    import uiconst
    import mathUtil
    import xtriui
    import uthread
    import form
    import blue
    import util
    import trinity
    import service
    import listentry
    import base
    import math
    import sys
    import geo2
    import maputils
    import state
    from math import pi, cos, sin, sqrt
    from foo import Vector3
    from mapcommon import SYSTEMMAP_SCALE
    import functools
    import uicls
    import localization
    import localizationUtil

    try:
        old_apply_attributes
    except NameError:
        old_apply_attributes = form.Scanner.ApplyAttributes

    def safetycheck(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                try:
                    print "exception in " + func.__name__
                    (exc, e, tb,) = sys.exc_info()
                    result2 = (''.join(format_exception(exc, e, tb)) + '\n').replace('\n', '<br>')
                    sm.GetService('gameui').MessageBox(result2, "ProbeHelper Exception")
                except:
                    print "exception in safetycheck"
        return wrapper

    @safetycheck
    def ContractProbes(self, *args):
        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        avg = sum( (v.destination for (k,v) in probeData), Vector3() ) / len(probeData)
        min_range = min( v.rangeStep for (k,v) in probeData )
        if (min_range <= 1):
            return

        for (k,v) in probeData:
            destination = (v.destination + avg)/2
            scanSvc.SetProbeDestination(k, destination)
            scanSvc.SetProbeRangeStep(k, v.rangeStep - 1)

        self.UpdateProbeSpheres()

    form.Scanner.ContractProbes = ContractProbes

    @safetycheck
    def ExpandProbes(self, *args):
        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        avg = sum( (v.destination for (k,v) in probeData), Vector3() ) / len(probeData)
        max_range = max( v.rangeStep for (k,v) in probeData )
        if (max_range >= const.scanProbeNumberOfRangeSteps):
            return

        for (k,v) in probeData:
            destination = (2 * v.destination) - avg
            scanSvc.SetProbeDestination(k, destination)
            scanSvc.SetProbeRangeStep(k, v.rangeStep + 1)

        self.UpdateProbeSpheres()

    form.Scanner.ExpandProbes = ExpandProbes


    @safetycheck
    def SendProbes(self, *args):
        selected = self.sr.resultscroll.GetSelected()
        try:
            data = selected[0].result.data
        except:
            return

        if isinstance(data, float):
            data = selected[0].result.pos

        if not isinstance(data, Vector3):
            data = data.point

        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        avg = sum( (v.destination for (k,v) in probeData), Vector3() ) / len(probeData)

        for (k,v) in probeData:
            destination = data + v.destination - avg
            scanSvc.SetProbeDestination(k, destination)

        self.UpdateProbeSpheres()

    form.Scanner.SendProbes = SendProbes

    @safetycheck
    def MoveScan(self, *args):
        target = settings.public.ui.Get('AItarget', False)
        if not target:
            return

        data = target.data
        if isinstance(data, float):
            data = target.pos

        if not isinstance(data, Vector3):
            data = data.point

        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        avg = sum( (v.destination for (k,v) in probeData), Vector3() ) / len(probeData)

        for (k,v) in probeData:
            destination = data + v.destination - avg
            scanSvc.SetProbeDestination(k, destination)

        self.UpdateProbeSpheres()

        self.Analyze(*args)

    form.Scanner.MoveScan = MoveScan

    @safetycheck
    def CustomPattern(self, *args):
        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        sm.GetService('gameui').Say(str(len(probeData)) + " probes detected")
        avg = sum( (v.destination for (k,v) in probeData), Vector3() ) / len(probeData)
        trans = sum(v.scanRange for (k,v) in probeData) / len(probeData)

        if len(probeData) in (4, 6):
            i = 1
            trans = trans*1/2
            patternType = "cube"
        else:
            i = 0
            trans = trans*3/4
            patternType = "star"
        sm.GetService('gameui').Say(str(len(probeData)) + " probes detected: " + patternType + " pattern")

        probePos = [
            [0, 0, 0],
            [-trans, 0, 0],
            [trans, 0, 0],
            [0, 0, -trans],
            [0, 0, trans],
            [0, -trans, 0],
            [0, trans, 0],
            [0, 0, 0],
            ]

        for (k,v) in probeData:
            destination = Vector3(probePos[i][0], probePos[i][1], probePos[i][2]) + avg
            scanSvc.SetProbeDestination(k, destination)
            i += 1
        self.UpdateProbeSpheres()

    form.Scanner.CustomPattern = CustomPattern

    @safetycheck
    def SaveLoadProbePositions(self, *args):
        scanSvc = sm.GetService("scanSvc")
        probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
        if len(probeData) == 0:
            return

        settingsName = 'ProbePositons2' if uicore.uilib.Key(uiconst.VK_CONTROL) else 'ProbePositions'

        if uicore.uilib.Key(uiconst.VK_SHIFT):
            settings.public.ui.Set(settingsName, [(v.destination, v.rangeStep) for (k,v) in probeData])
            return

        pos = settings.public.ui.Get(settingsName, [])
        if( pos == [] ):
            return

        for ((probekey, probevalue), (dest,rstep)) in zip(probeData, pos):
            scanSvc.SetProbeDestination(probekey, dest)
            scanSvc.SetProbeRangeStep(probekey, rstep)
        self.UpdateProbeSpheres()

    form.Scanner.SaveLoadProbePositions = SaveLoadProbePositions

    @safetycheck
    def Nuke(self, *args):
        probes = sm.GetService('scanSvc').GetActiveProbes()
        if probes:
            if settings.public.ui.Get('AIscanActive', False):
                settings.public.ui.Set('AIscanActive', False)
                sm.GetService('gameui').Say("AI scan stopped")
                self.sr.nukeBtn.sr.icon.LoadIcon('44_17')
            else:
                selected = self.sr.resultscroll.GetSelected()
                if len(selected)>1:
                    sm.GetService('gameui').Say("Select only one target")
                    return
                elif len(selected)<1:
                    sm.GetService('gameui').Say("Select target")
                    return

                try:
                    target = selected[0].result.data
                except:
                    sm.GetService('gameui').Say("Select target")
                    return

                target = selected[0].result
                try:
                    scanSvc = sm.GetService('scanSvc')
                    probeData = [(k,v) for (k,v) in scanSvc.GetProbeData().iteritems() if v.state == const.probeStateIdle]
                    if len(probeData) == 0:
                        return

                    max_range = max( v.rangeStep for (k,v) in probeData )

                    for (k,v) in probeData:
                        scanSvc.SetProbeRangeStep(k, max_range)
                    self.UpdateProbeSpheres()

                    settings.public.ui.Set('AItarget', target)
                    self.CustomPattern()
                    self.MoveScan()
                    sm.GetService('gameui').Say("AI scan started")
                    settings.public.ui.Set('AIscanActive', True)
                    self.sr.nukeBtn.sr.icon.LoadIcon('44_19')
                except:
                    sm.GetService('gameui').Say("Probes are busy!")
                    return
        else:
            sm.GetService('gameui').Say("You need active probes")

    form.Scanner.Nuke = Nuke

    @safetycheck
    def MyCheckButtonStates(self):
        probes = sm.GetService('scanSvc').GetActiveProbes()
        scanningProbes = sm.GetService('scanSvc').GetScanningProbes()
        allIdle = self.ValidateProbesState(probes)
        if probes and allIdle:
            self.sr.destroyBtn.Enable()
            self.sr.recoverBtn.Enable()
        else:
            self.sr.destroyBtn.Disable()
            self.sr.destroyBtn.opacity = 0.25
            self.sr.recoverBtn.Disable()
            self.sr.recoverBtn.opacity = 0.25
        canClaim = sm.GetService('scanSvc').CanClaimProbes()
        if canClaim:
            self.sr.reconnectBtn.Enable()
        else:
            self.sr.reconnectBtn.Disable()
            self.sr.reconnectBtn.opacity = 0.25
        if scanningProbes:
            self.sr.analyzeBtn.Disable()
            self.sr.analyzeBtn.opacity = 0.25
        else:
            self.sr.analyzeBtn.Enable()
        if self.sr.nukeBtn:
            if settings.public.ui.Get('AIscanActive', False):
                self.sr.nukeBtn.sr.icon.LoadIcon('44_19')
            else:
                self.sr.nukeBtn.sr.icon.LoadIcon('44_17')

    form.Scanner.CheckButtonStates = MyCheckButtonStates

    @safetycheck
    def MyLoadResultList(self):
        if self.destroyed:
            return
        scanSvc = sm.GetService('scanSvc')
        currentScan = scanSvc.GetCurrentScan()
        scanningProbes = scanSvc.GetScanningProbes()
        results = scanSvc.GetScanResults()
        if self.destroyed:
            return
        self.CleanupResultShapes()
        resultlist = []
        if currentScan and blue.os.TimeDiffInMs(currentScan.startTime, blue.os.GetSimTime()) < currentScan.duration:
            data = util.KeyVal()
            data.header = localization.GetByLabel('UI/Inflight/Scanner/Analyzing')
            data.startTime = currentScan.startTime
            data.duration = currentScan.duration
            resultlist.append(listentry.Get('Progress', data=data))
        elif scanningProbes and session.shipid not in scanningProbes:
            data = util.KeyVal()
            data.label = localization.GetByLabel('UI/Inflight/Scanner/WaitingForProbes')
            data.hideLines = 1
            resultlist.append(listentry.Get('Generic', data=data))
        elif results:

            if settings.public.ui.Get('AIscanActive', False) and settings.public.ui.Get('AItarget', False):
                keyFound = False
                for result in results:
                    if settings.public.ui.Get('AItarget', False).id == result.id:
                        keyFound = True
                        if result.certainty < 1:
                            self.ContractProbes()
                            settings.public.ui.Set('AItarget', result)
                            self.MoveScan()
                        else:
                            settings.public.ui.Set('AIscanActive', False)
                            sm.GetService('gameui').Say("AI scan finished")
                            self.sr.nukeBtn.sr.icon.LoadIcon('44_17')
                if not(keyFound):
                    settings.public.ui.Set('AIscanActive', False)
                    sm.GetService('gameui').Say("AI can't find signal")
                    self.sr.nukeBtn.sr.icon.LoadIcon('44_17')

            bp = sm.GetService('michelle').GetBallpark(doWait=True)
            if bp is None:
                return
            ego = bp.balls[bp.ego]
            myPos = Vector3(ego.x, ego.y, ego.z)
            for result in results:
                if result.id in scanSvc.resultsIgnored:
                    continue
                if self.currentFilter is not None and len(self.currentFilter) > 0:
                    if result.certainty >= const.probeResultGood:
                        if result.groupID == const.groupCosmicSignature:
                            if (result.groupID, result.strengthAttributeID) not in self.currentFilter:
                                continue
                        elif result.groupID not in self.currentFilter:
                            continue
                    elif result.scanGroupID not in self.activeScanGroupInFilter:
                        continue
                id = result.id
                probeID = result.probeID
                certainty = result.certainty
                typeID = result.Get('typeID', None)
                result.typeName = None
                result.groupName = None
                result.scanGroupName = self.scanGroups[result.scanGroupID]
                if certainty >= const.probeResultGood:
                    explorationSite = result.groupID in (const.groupCosmicAnomaly, const.groupCosmicSignature)
                    if explorationSite:
                        result.groupName = self.GetExplorationSiteType(result.strengthAttributeID)
                    else:
                        result.groupName = cfg.invgroups.Get(result.groupID).name
                    if certainty >= const.probeResultInformative:
                        if explorationSite:
                            result.typeName = result.dungeonName
                        else:
                            result.typeName = cfg.invtypes.Get(typeID).name
                if isinstance(result.data, Vector3):
                    dist = (result.data - myPos).Length()
                elif isinstance(result.data, float):
                    dist = result.data
                    certainty = min(0.9999, certainty)
                else:
                    dist = (result.data.point - myPos).Length()
                    certainty = min(0.9999, certainty)
                texts = [result.id,
                 result.scanGroupName,
                 result.groupName or '',
                 result.typeName or '',
                 localization.GetByLabel('UI/Inflight/Scanner/SignalStrengthPercentage', signalStrength=min(1.0, certainty) * 100),
                 util.FmtDist(dist)]
                sortdata = [result.id,
                 result.scanGroupName,
                 result.groupName or '',
                 result.typeName or '',
                 min(1.0, certainty) * 100,
                 dist]
                data = util.KeyVal()
                data.texts = texts
                data.sortData = sortdata
                data.columnID = 'probeResultGroupColumn'
                data.result = result
                data.GetMenu = self.ResultMenu
                resultlist.append(listentry.Get('ColumnLine', data=data))

            resultlist = listentry.SortColumnEntries(resultlist, 'probeResultGroupColumn')
            data = util.KeyVal()
            data.texts = [localization.GetByLabel('UI/Common/ID'),
             localization.GetByLabel('UI/Inflight/Scanner/ScanGroup'),
             localization.GetByLabel('UI/Inventory/ItemGroup'),
             localization.GetByLabel('UI/Common/Type'),
             localization.GetByLabel('UI/Inflight/Scanner/SignalStrength'),
             localization.GetByLabel('UI/Common/Distance')]
            data.columnID = 'probeResultGroupColumn'
            data.editable = True
            data.showBottomLine = True
            data.selectable = False
            data.hilightable = False
            resultlist.insert(0, listentry.Get('ColumnLine', data=data))
            listentry.InitCustomTabstops(data.columnID, resultlist)
        if not resultlist:
            data = util.KeyVal()
            data.label = localization.GetByLabel('UI/Inflight/Scanner/NoScanResult')
            data.hideLines = 1
            resultlist.append(listentry.Get('Generic', data=data))
        resultlist.append(listentry.Get('Line', data=util.KeyVal(height=1)))
        self.sr.resultscroll.Load(contentList=resultlist)
        self.sr.resultscroll.ShowHint('')
        self.HighlightGoodResults()

    form.Scanner.LoadResultList = MyLoadResultList

    @safetycheck
    def MyApplyAttributes(self, attributes):
        old_apply_attributes(self, attributes)

        self.sr.destroyBtn.Close()

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=120)
        btn.OnClick = self.CustomPattern
        btn.hint = "Send Probes to Custom Pattern"
        btn.sr.icon.LoadIcon('77_32')
        self.sr.customBtn = btn

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=164)
        btn.OnClick = self.SendProbes
        btn.hint = "Send Probes to Selected Result"
        btn.sr.icon.LoadIcon('44_59')
        self.sr.sendBtn = btn

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=200)
        btn.OnClick = self.ContractProbes
        btn.hint = "Shrink Probe Pattern"
        btn.sr.icon.LoadIcon('44_43')
        self.sr.contractBtn = btn

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=236)
        btn.OnClick = self.ExpandProbes
        btn.hint = "Expand Probe Pattern"
        btn.sr.icon.LoadIcon('44_44')
        self.sr.expandBtn = btn

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=280)
        btn.OnClick = self.SaveLoadProbePositions
        btn.hint = "Load Probe Positions\nShift-Click to Save Probe Positions"
        btn.sr.icon.LoadIcon('44_03')
        self.sr.saveloadBtn = btn

        btn = uix.GetBigButton(32, self.sr.systemTopParent, left=316)
        btn.OnClick = self.Nuke
        btn.hint = "AI scan"
        btn.sr.icon.LoadIcon('44_17')
        self.sr.nukeBtn = btn

    form.Scanner.ApplyAttributes = MyApplyAttributes

except:
    print "ProbeHelper broken."
